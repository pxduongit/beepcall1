/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 *
 * @author Duongpx2
 */
public class ChangePasswordDTO {

    @JsonProperty(value = "old_pass")
    private String oldPass;

    @JsonProperty(value = "new_pass")
    private String newPass;

    @JsonProperty(value = "re_new_pass")
    private String reNewPass;

    public String getOldPass() {
        return oldPass;
    }

    public void setOldPass(String oldPass) {
        this.oldPass = oldPass;
    }

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }

    public String getReNewPass() {
        return reNewPass;
    }

    public void setReNewPass(String reNewPass) {
        this.reNewPass = reNewPass;
    }

}
