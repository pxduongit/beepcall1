/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.dto;

import com.cms.xsonline.base.BaseDTO;

/**
 *
 * @author Duongpx2
 */
public class MTReportDTO extends BaseDTO {

    private String phoneNumber;
    private String mtSentTime;
    private String content;
    private boolean charging;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getMtSentTime() {
        return mtSentTime;
    }

    public void setMtSentTime(String mtSentTime) {
        this.mtSentTime = mtSentTime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isCharging() {
        return charging;
    }

    public void setCharging(boolean charging) {
        this.charging = charging;
    }


}
