/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.dto;

import com.cms.xsonline.base.BaseDTO;

/**
 *
 * @author Duongpx2
 */
public class MOReportDTO extends BaseDTO {

    private String date;
    private Long totalMO;
    private Long totalMOSuccess;
    private Long totalMOWrongSyntax;
    private Long totalMOHelp;
    private Long totalOutOfMoney;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Long getTotalMO() {
        return totalMO;
    }

    public void setTotalMO(Long totalMO) {
        this.totalMO = totalMO;
    }

    public Long getTotalMOSuccess() {
        return totalMOSuccess;
    }

    public void setTotalMOSuccess(Long totalMOSuccess) {
        this.totalMOSuccess = totalMOSuccess;
    }

    public Long getTotalMOWrongSyntax() {
        return totalMOWrongSyntax;
    }

    public void setTotalMOWrongSyntax(Long totalMOWrongSyntax) {
        this.totalMOWrongSyntax = totalMOWrongSyntax;
    }

    public Long getTotalMOHelp() {
        return totalMOHelp;
    }

    public void setTotalMOHelp(Long totalMOHelp) {
        this.totalMOHelp = totalMOHelp;
    }

    public Long getTotalOutOfMoney() {
        return totalOutOfMoney;
    }

    public void setTotalOutOfMoney(Long totalOutOfMoney) {
        this.totalOutOfMoney = totalOutOfMoney;
    }

}
