/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.dto;

import com.cms.xsonline.base.BaseQueryDTO;

/**
 *
 * @author pxduongit
 */
public class BranchDetailQueryDTO extends BaseQueryDTO {

    private String phoneNumber;
    private String name;
    private String description;
    private String isActive;
    private String phoneNumberExclude;
    private String userId;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }

    public String getPhoneNumberExclude() {
        return phoneNumberExclude;
    }

    public void setPhoneNumberExclude(String phoneNumberExclude) {
        this.phoneNumberExclude = phoneNumberExclude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    

}
