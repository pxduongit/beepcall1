/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.dto;

import com.cms.xsonline.base.BaseDTO;
import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author pxduong
 */
public class AccountDTO extends BaseDTO implements Serializable {

    private Long id;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String password;
    private String rePassword;
    private String rememberMe;
    private String activeCode;
    private String passwordCode;
    private Boolean enable;
    private String modificationTime;
    private String creationTime;
    private String phone;
    private List<String> listRoleName;//ten quyen
    private List<GrantedAuthority> listRoles;//dung khi authen
    private String role;//tam thoi fix cung khi lay tu man hinh them moi. mo rong them se cho phep chon //ID quyen
    private String roleName;
    private Integer OTPPass;
    private Date OTPPassValidTime;
    private boolean isAdminChangePass;
    //OTP confirm payment
    private boolean isLocked;
    //thong tin agent
    private String address;
    private Long agentId;
    private String agentCode;
    private String agentName;
//    private Float discountPercent;
    private String salt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRePassword() {
        return rePassword;
    }

    public void setRePassword(String rePassword) {
        this.rePassword = rePassword;
    }

    public String getRememberMe() {
        return rememberMe;
    }

    public void setRememberMe(String rememberMe) {
        this.rememberMe = rememberMe;
    }

    public String getActiveCode() {
        return activeCode;
    }

    public void setActiveCode(String activeCode) {
        this.activeCode = activeCode;
    }

    public String getPasswordCode() {
        return passwordCode;
    }

    public void setPasswordCode(String passwordCode) {
        this.passwordCode = passwordCode;
    }

    public Boolean getEnable() {
        return enable;
    }

    public void setEnable(Boolean enable) {
        this.enable = enable;
    }

    public String getModificationTime() {
        return modificationTime;
    }

    public void setModificationTime(String modificationTime) {
        this.modificationTime = modificationTime;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public List<GrantedAuthority> getListRoles() {
        return listRoles;
    }

    public void setListRoles(List<GrantedAuthority> listRoles) {
        this.listRoles = listRoles;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Integer getOTPPass() {
        return OTPPass;
    }

    public void setOTPPass(Integer OTPPass) {
        this.OTPPass = OTPPass;
    }

    public Date getOTPPassValidTime() {
        return OTPPassValidTime;
    }

    public void setOTPPassValidTime(Date OTPPassValidTime) {
        this.OTPPassValidTime = OTPPassValidTime;
    }

    public boolean isIsAdminChangePass() {
        return isAdminChangePass;
    }

    public void setIsAdminChangePass(boolean isAdminChangePass) {
        this.isAdminChangePass = isAdminChangePass;
    }

    public List<String> getListRoleName() {
        return listRoleName;
    }

    public void setListRoleName(List<String> listRoleName) {
        this.listRoleName = listRoleName;
    }

    public boolean isIsLocked() {
        return isLocked;
    }

    public void setIsLocked(boolean isLocked) {
        this.isLocked = isLocked;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

}
