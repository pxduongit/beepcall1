package com.cms.xsonline.facade;

import com.cms.xsonline.dto.AccountDTO;
import com.cms.xsonline.obj.exception.FAException;
import com.cms.xsonline.obj.request.FAInformRequest;
import com.cms.xsonline.service.AccountService;
import com.cms.xsonline.service.BranchService;
import com.cms.xsonline.util.Constant;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AlarmFacade {

    private final Logger logger = LoggerFactory.getLogger(AlarmFacade.class);

    @Autowired
    private AccountService accountService;

    @Autowired
    private BranchService branchService;

    public int makeCall(FAInformRequest informRequest, long currentUser) throws FAException {
        logger.info("makecCall from building management |=========>>>");
        AccountDTO accountDTO = accountService.getById(currentUser);
        if (accountDTO == null) {
            throw new FAException(Constant.RESPONSE_CODE_FAILED, "You do not have permission to inform alarm");
        }
        try {
            String notifyContent = informRequest.note;
            String result = branchService.makeCall(informRequest.buildingId, notifyContent, accountDTO);

            return "SUCCESS".equalsIgnoreCase(result) ? 1 : 0;
        } catch (Exception e) {
            throw e;
        }
    }

    public int settingAlarm(int userId, boolean enableOrNot) throws FAException {
        logger.info("settingAlarm for user |====================>>>>> userId = {} enableOrNot = {}", userId, enableOrNot);
        try {
            return accountService.activeNotification(userId, enableOrNot);
        } catch (Exception e) {
            throw e;
        }
    }
}
