package com.cms.xsonline.facade;

import com.cms.xsonline.dto.AccountDTO;
import com.cms.xsonline.dto.BranchDTO;
import com.cms.xsonline.dto.ChangePasswordDTO;
import com.cms.xsonline.dto.RoleDTO;
import com.cms.xsonline.obj.exception.FAException;
import com.cms.xsonline.obj.request.FAUpdateRequest;
import com.cms.xsonline.obj.response.LoginResponse;
import com.cms.xsonline.obj.response.ManagerPhoneResponse;
import com.cms.xsonline.service.AccountService;
import com.cms.xsonline.service.BranchService;
import com.cms.xsonline.util.Constant;
import com.cms.xsonline.util.MD5Util;
import com.cms.xsonline.util.TokenUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class LoginFacade {

    @Autowired
    AccountService accountService;

    @Autowired
    BranchService branchService;

    public LoginResponse login(String username, String password) throws FAException {
        try {
            if (Strings.isNullOrEmpty(username) || Strings.isNullOrEmpty(password)) {
                throw new IllegalArgumentException("Params must be not empty");
            }

            List<AccountDTO> accountDTOs = accountService.getByUserName(username);
            if (CollectionUtils.isEmpty(accountDTOs)) {
                throw new FAException(Constant.ERROR_CODE_USER_NOT_EXIST, "This account is not existed in system. Please contact the system admin");
            }
            AccountDTO accountDTO = accountDTOs.get(0);
            String MD5Pass = MD5Util.getSecurePassword(password.concat(Strings.isNullOrEmpty(accountDTO.getSalt()) ? "" : accountDTO.getSalt()));
            if (!MD5Pass.equalsIgnoreCase(accountDTO.getPassword())) {
                throw new FAException(Constant.ERROR_CODE_PASSWORD_INCORRECT, "Password is wrong");
            }
            LoginResponse loginResponse = new LoginResponse();
            loginResponse.username = accountDTO.getUsername();
            loginResponse.email = accountDTO.getEmail();
            loginResponse.phone = accountDTO.getPhone();
            loginResponse.firstName = accountDTO.getFirstName();
            loginResponse.lastName = accountDTO.getLastName();

            List<RoleDTO> listRoles = this.accountService.getRoleByUserId(accountDTO.getId());
            if (!CollectionUtils.isEmpty(listRoles)) {
                Map<String, String> role_ = Maps.newConcurrentMap();
                for (RoleDTO role : listRoles) {
                    role_.put(role.getRoles(), role.getRoleName());
                }
                //Need to update data if you want to dynamic role
                loginResponse.role = role_;
                loginResponse.roleAdmin = role_.containsKey("ROLE_AGENT") ? 1 : 0;
            }

            //Apartment need to update database
            List<BranchDTO> buildings = branchService.getById(accountDTO.getAgentId());
            if (!CollectionUtils.isEmpty(buildings)) {
                loginResponse.building = buildings.get(0).getName();
                loginResponse.buildingAddress = buildings.get(0).getAddr();
                loginResponse.buildingCode = buildings.get(0).getCode();
                loginResponse.buildingId = buildings.get(0).getId();
            }
            //fix manager phone
            List<AccountDTO> listManager = this.accountService.getManagerByAgent(accountDTO.getAgentId());
            List<ManagerPhoneResponse> managerPhone = new ArrayList<>();
            for (AccountDTO manager : listManager) {
                ManagerPhoneResponse temp = new ManagerPhoneResponse();
                temp.setAdmin(manager.getLastName() == null ? "" : manager.getLastName()
                        .concat(" ")
                        .concat(manager.getFirstName() == null ? "" : manager.getFirstName()));
                temp.setPhone(manager.getPhone());
                managerPhone.add(temp);
            }
            loginResponse.managerPhone = managerPhone;
            //Need to generate token here
            loginResponse.accessToken = TokenUtils.createTokenForUser(accountDTO.getId(), accountDTO.getUsername(), TokenUtils.EXPIRE_TIME);
            //TODO: if you need to store token to trace log please save it into USER table

            return loginResponse;
        } catch (FAException e) {
            throw e;
        }
    }

    public int changePassword(int userId, ChangePasswordDTO changeRequest) throws FAException {
        if (Strings.isNullOrEmpty(changeRequest.getOldPass()) || Strings.isNullOrEmpty(changeRequest.getNewPass())) {
            throw new IllegalArgumentException("Params must be not empty");
        }
        if (!changeRequest.getNewPass().equals(changeRequest.getReNewPass())) {
            throw new IllegalArgumentException("Re password is not matching");
        }
        AccountDTO accountDTO = accountService.getById((long) userId);
        if (accountDTO != null) {
            return accountService.changePass(accountDTO, changeRequest);
        }
        return 1;
    }

    public int updateProfile(int currentUser, FAUpdateRequest updateRequest) throws FAException {

        try {
            if (Strings.isNullOrEmpty(updateRequest.phone)) {
                throw new IllegalArgumentException("Params must be not empty");
            }

            return accountService.updateProfile(currentUser, updateRequest);
        } catch (Exception e) {
            throw e;
        }
    }
}
