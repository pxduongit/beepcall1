/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.util;

/**
 *
 * @author pxduong
 */
public class Constant {

    public static String sqlDateTimeFormat = "YYYY-MM-DD HH24:MI:SS";
    public static String shortSqlDateTimeFormat = "yyyy-MM-dd";

    public static String sqlDateTimeFormat2 = "MM/DD/YYYY HH24:MI:SS";

    public static Long MAX_LOGIN_FAIL = 5L;

    public static int USER_SALT_LENGTH = 16;

    public static class PaymentConfirmErrorCode {

        public final static String SUCCESS = "SUCCESS";
        public final static String WRONG_OTP = "WRONG_OTP";
        public final static String WRONG_OTP_LOCKED = "WRONG_OTP_LOCKED";
        public final static String UNKNOWN = "UNKNOWN";
        public final static String ACCOUNT_LOCKED = "ACCOUNT_LOCKED";
        public final static String EMPTY_OTP = "EMPTY_OTP";
    }

    public static class CallOutEventStatus {

        public final static String INIT = "I";
    }

    public static class SessionKey {

        public final static String OTP_FAIL_COUNTER = "FAIL_COUNTER";
        public final static String OTP_CONFIRM_PAYMENT = "OTP_CONFIRM_PAYMENT";
        public final static String LOGIN_FAIL_COUNTER = "LOGIN_FAIL_COUNTER";
        public final static String USED_CAPTCHA = "captchaTokenUsed";
        public final static String CAPTCHA_TOKEN = "captchaToken";
    }

    public static class ErrorCode {

        public final static String SUCCESS = "00";
        public final static String UNKNOWN = "99";
        public final static String INVALIDATE_DATA = "98";
        public final static String HAS_NO_ACCESS = "97";
        public final static String WRONG_DATA = "96";
        public final static String DUPLICATE_DATA = "95";
    }

    public static class ErrorMessage {

        public final static String SUCCESS = "Success!";
        public final static String UNKNOWN = "Have error while processing. Please try again later...";
        public final static String INVALIDATE_DATA = "The input is not correct!";
    }

    public static String ROLE_AGENT = "ROLE_AGENT";

    public static String ROLE_SUPER_AGENT_ID = "5";
    public static String ROLE_AGENT_ID = "6";

    //
    public static String AREA_CODE = "670";


    //NamNV -- BEGIN 2018.04.24
    public static int RESPONSE_CODE_OK = 200;
    public static int RESPONSE_CODE_FAILED = 404;
    public static int RESPONSE_CODE_UNAUTHORIZED = 401;
    public static int RESPONSE_CODE_TIMEOUT = 440;

    public static int RESPONSE_CODE_NOT_PAY_MONEY = 405;
    public static int RESPONSE_CODE_EXPIRED_TRIAL_TIME = 410;
    public static int ERROR_CODE_USER_LIMITED = 411;
    public static int ERROR_CODE_UNSPECIFIED = 999999;
    public static int ERROR_CODE_EMPTY_PARAM = 100001;
    public static int ERROR_CODE_INVALID_PARAM = 100002;
    public static int ERROR_CODE_USER_NOT_EXIST = 100005;
    public static int ERROR_CODE_PASSWORD_INCORRECT = 100010;
    public static int ERROR_CODE_TOKEN_INVALID = 100006;
    public static int ERROR_CODE_TOKEN_EXPIRED = 100007;


    public static int ERROR_CODE_ENTITY_EXISTED = 100;

    public static String DATE_TIME_FORMAT_4_WEB_DISPLAY = "MM-dd-yyyy HH:mm";
}
