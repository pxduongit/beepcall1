/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.dao;

import com.cms.xsonline.base.BaseDAO;
import com.cms.xsonline.dto.AccountDTO;
import com.cms.xsonline.dto.LotteryQueryDTO;
import com.cms.xsonline.dto.LotteryResultDTO;
import com.cms.xsonline.dto.PlayerDTO;
import com.cms.xsonline.util.Constant;
import com.cms.xsonline.util.Utilities;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

/**
 *
 * @author pxduong
 */
@Repository
public class CallOutEventDAO extends BaseDAO {

    public Integer insertPokeCall(PlayerDTO result) {///return userId
        try {
            StringBuilder queryString = new StringBuilder();
            queryString.append("INSERT INTO POKE_CALL " + "(trans_id,msisdn,request_time,res,retry_total,package_id,system_status,row_id) " + "VALUES " + "(?, ?,  " + " TO_DATE(?, '").append(Constant.sqlDateTimeFormat).append("'), ?,?,?,?,?)");
            int rs = getJdbcTemplate().update(queryString.toString(), new Object[]{
                result.getTransId(),
                result.getPhoneNumber(),
                Utilities.getDateStr(result.getRequestTime()),
                result.getRes(),
                result.getRetryTotal(),
                result.getPackageId(),
                result.getSystemStatus(),
                result.getRowId()
            });

            return rs;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    public void saveBatch(final List<PlayerDTO> playerDTOs, final Long insertedId) {

        StringBuilder queryString = new StringBuilder();
        queryString.append("INSERT INTO POKE_CALL " + "(trans_id,msisdn,request_time,res,retry_total, "
                + "package_id,system_status,row_id, notify_content) "
                + "VALUES " + "(?, ?,  " + " TO_DATE(?, '").append(Constant.sqlDateTimeFormat).append("'), ?,?,?,?,?, ?)");
        String QUERY_SAVE = queryString.toString();
        getJdbcTemplate().batchUpdate(QUERY_SAVE,
                new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i)
                    throws SQLException {
                PlayerDTO bo = playerDTOs.get(i);
                ps.setString(1, bo.getTransId());
                ps.setString(2, bo.getPhoneNumber());
                ps.setString(3, Utilities.getDateStr(bo.getRequestTime()));
                ps.setString(4, bo.getRes() == null ? "0" : bo.getRes());
                ps.setInt(5, bo.getRetryTotal() == null ? 0 : bo.getRetryTotal());
                ps.setLong(6, insertedId);
                ps.setString(7, bo.getSystemStatus() == null ? "" : bo.getSystemStatus());
                ps.setLong(8, bo.getRowId());
                ps.setString(9, bo.getNoitifyContent() == null ? "" : bo.getNoitifyContent());
            }

            @Override
            public int getBatchSize() {
                return playerDTOs.size();
            }

        });

    }

    public Integer updateTotalRow(int totalRow, long id) {
        String queryString = "update d_g_file set total_row = " + totalRow + " where id = " + id;
        try {
            return getJdbcTemplate().update(queryString);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public Integer insertResult(AccountDTO accountInfo, LotteryResultDTO result) {///return userId
        try {
            int insertedId = this.getGFileSeqNextval();
            StringBuilder queryString = new StringBuilder();
            queryString.append("INSERT INTO D_G_FILE " + "(id, file_name, real_file_name, total_row,"
                    + " total_commit_row, status, UPDATED_DATE, UPDATED_BY, AGENT_ID) "
                    + "VALUES " + "(?, ?, ?,?,?,?, " + " TO_DATE(?, '").append(Constant.sqlDateTimeFormat).append("')"
                    + ", ?, ?)");
            Object[] params = new Object[]{
                insertedId,
                result.getFileName(),
                result.getRealFileName(),
                result.getTotalRow(),
                0,
                result.getStatus(),
                Utilities.getDateStr(new Date(System.currentTimeMillis())),
                accountInfo.getId(),
                result.getBranchId()
            };

            getJdbcTemplate().update(queryString.toString(), params);
            return insertedId;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    public Integer getGFileSeqNextval() {
        String queryString = "select D_g_file_seq.nextval id from dual";
        try {
            SqlRowSet dataRow = getJdbcTemplate().queryForRowSet(queryString);
            if (dataRow != null && dataRow.next()) {
                return dataRow.getInt("id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
//
//    public Integer UpdateResult(AccountDTO accountInfo, LotteryResultDTO result) {///return userId
//        try {
//            StringBuilder queryString = new StringBuilder();
//            queryString.append("UPDATE LOTTERY_RESULT SET "
//                    + "RESULT_CODE = ?"
//                    + ", UPDATED_DATE = TO_DATE(?,'" + Constant.sqlDateTimeFormat + "')"
//                    + ", UPDATED_BY = ? "
//                    + " WHERE ID = ?");
//
//            int rs = getJdbcTemplate().update(queryString.toString(), new Object[]{
//                result.getResultCode(),
//                Utilities.getDateStr(new Date()),
//                accountInfo.getId(),
//                result.getId()
//            });
//            return rs;
//        } catch (Exception ex) {
//            System.out.println(ex.getMessage());
//        }
//
//        return 0;
//    }
//
//    public Integer CommitResult(AccountDTO accountInfo, Long id) {///return userId
//        try {
//            StringBuilder queryString = new StringBuilder();
//            queryString.append("UPDATE LOTTERY_RESULT SET "
//                    + "IS_COMMITED = 1"
//                    + ", UPDATED_DATE = TO_DATE(?,'" + Constant.sqlDateTimeFormat + "')"
//                    + ", UPDATED_BY = ? "
//                    + " WHERE ID = ?");
//
//            return getJdbcTemplate().update(queryString.toString(), new Object[]{
//                Utilities.getDateStr(new Date()),
//                accountInfo.getId(),
//                id
//            });
//        } catch (Exception ex) {
//            System.out.println(ex.getMessage());
//        }
//        return 0;
//    }
//
//    public LotteryResultDTO getIdByDate(String date) {///return userId
//        LotteryResultDTO result = new LotteryResultDTO();
//        try {
//            StringBuilder queryString = new StringBuilder();
//            queryString.append("SELECT ID, UPDATED_BY, IS_COMMITED FROM LOTTERY_RESULT WHERE TO_CHAR(DATE_TIME,'MM/DD/YYYY') = ?");
//            SqlRowSet dataRow = null;
//            try {
//                dataRow = getJdbcTemplate().queryForRowSet(queryString.toString(), new Object[]{date});
//            } catch (Exception ex) {
//                //log
//            }
//            if (dataRow != null && dataRow.next()) {
//                result.setId(dataRow.getLong("ID"));
//                result.setUpdatedBy(dataRow.getLong("UPDATED_BY"));
//                result.setStatus(dataRow.getInt("STATUS"));
//            }
//        } catch (Exception ex) {
//            System.out.println(ex.getMessage());
//        }
//        return result;
//    }

    public Integer getCount(LotteryQueryDTO query) {
        Object[] strQuery = getStrQuery(1, query);
        if (strQuery != null) {
            String queryString = strQuery[0].toString();
            Object[] params = (Object[]) strQuery[1];
            SqlRowSet dataRow = null;
            try {
                dataRow = getJdbcTemplate().queryForRowSet(queryString, params);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
            if (dataRow != null && dataRow.next()) {
                return dataRow.getInt("COUNT");
            }
        }
        return 0;
    }

    public List<LotteryResultDTO> getByPage(LotteryQueryDTO query) {
        List<LotteryResultDTO> result = new ArrayList<>();
        Object[] strQuery = getStrQuery(0, query);
        if (strQuery != null) {
            String queryString = strQuery[0].toString();
            Object[] params = (Object[]) strQuery[1];
            SqlRowSet dataRow = null;
            try {
                dataRow = getJdbcTemplate().queryForRowSet(queryString, params);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
            result = extractBranch(dataRow);
        }
        return result;
    }

    private List<LotteryResultDTO> extractBranch(SqlRowSet dataRow) {
        List<LotteryResultDTO> result = new ArrayList<>();
        while (dataRow != null && dataRow.next()) {
            LotteryResultDTO temp = new LotteryResultDTO();
            temp.setId(dataRow.getLong("ID"));
            temp.setTotalRow(dataRow.getLong("TOTAL_ROW"));
//            temp.setFileName(dataRow.getString("FILE_NAME"));
            temp.setUpdatedByName(dataRow.getString("UPDATED_BY_NAME"));
            //String updatedDate = Utilities.getDateStr(dataRow.getDate("UPDATED_DATE"));
            String updatedDate = dataRow.getString("UPDATED_DATE");
            String createdDate = dataRow.getString("CREATED_DATE");
            temp.setTotalCommitRow(dataRow.getLong("TOTAL_COMMIT_ROW"));
            temp.setUpdatedDate(updatedDate);
            temp.setCreatedDate(createdDate);
            temp.setStatus(dataRow.getString("STATUS"));
            temp.setExecutedDuration(dataRow.getInt("EXECUTED_DURATION"));
            temp.setCreatedBy(dataRow.getLong("CREATED_BY"));
            temp.setCreatedByName(dataRow.getString("CREATED_BY_NAME"));
            temp.setBranchId(dataRow.getLong("BRANCH_ID"));
            temp.setBranchName(dataRow.getString("BRANCH_NAME"));
            result.add(temp);
        }
        return result;
    }

    private Object[] getStrQuery(int type, LotteryQueryDTO query) {
        StringBuilder sql = new StringBuilder();
        List<Object> params = new ArrayList<>();
        switch (type) {
            case 1:
                //count
                sql.append(" SELECT COUNT(*) COUNT FROM (");
                break;
            case 0:
                //select *
                sql.append("SELECT * FROM (SELECT ROWNUM RNUM, A.*  FROM (");
                break;
            default:
                return null;
        }

        sql.append("SELECT L.ID, L.TOTAL_ROW,L.TOTAL_COMMIT_ROW,"
                + " UC.USER_ID CREATED_BY,"
                + " UC.USER_NAME CREATED_BY_NAME, "
                + " U.USER_NAME UPDATED_BY_NAME,"
                + " to_char(L.UPDATED_DATE,'dd/MM/yyyy hh24:mi:ss') UPDATED_DATE,"
                + " to_char(L.CREATED_DATE,'dd/MM/yyyy hh24:mi:ss') CREATED_DATE,"
                + " (case when L.done_date is null then 0 else round((L.done_date - L.updated_date)*86400) end)"
                + " EXECUTED_DURATION, L.STATUS, A.AGENT_ID BRANCH_ID, A.AGENT_NAME BRANCH_NAME  "
                + "FROM G_FILE L "
                + " LEFT JOIN D_USERS U ON L.UPDATED_BY = U.USER_ID "
                + " LEFT JOIN D_USERS UC ON L.CREATED_BY = UC.USER_ID "
                + " LEFT JOIN D_AGENTS A ON A.AGENT_ID = L.AGENT_ID "
                + "WHERE 1=1 ");
        if (query.getFromDate() != null) {
            sql.append(" AND L.CREATED_DATE >= TO_DATE(?,'").append(Constant.sqlDateTimeFormat).append("') ");
            params.add(Utilities.getDateStr(query.getFromDate()));
        }
        if (query.getToDate() != null) {
            sql.append(" AND L.CREATED_DATE <= TO_DATE(?,'").append(Constant.sqlDateTimeFormat).append("') ");
            params.add(Utilities.getDateStr(query.getToDate()));
        }
        if (query.getAgentName() != null) {
            sql.append(" AND A.AGENT_NAME like ? ");
            params.add("%" + query.getAgentName() + "%");
        }
        if (query.getAgentId() != null) {
            sql.append(" AND L.AGENT_ID = ? ");
            params.add(query.getAgentId());
        }
        sql.append(" ORDER BY L.CREATED_DATE DESC ");
        if (type == 0) {//chi co lay thong tin moi can loc theo so trang
            if (query.getCurPage() != null && query.getCurPage() > 0 && query.getPageSize() != null && query.getPageSize() > 0) {
                int from = (query.getPageSize() * (query.getCurPage() - 1)) + 1;
                int to = from + query.getPageSize() - 1;
                sql.append(") A WHERE ROWNUM<=?) WHERE RNUM>=?");
                params.add(to);
                params.add(from);
            } else {
                sql.append(") A)");
            }
        } else {
            sql.append(")");
        }
//        System.out.println("sql = " + sql);
        return new Object[]{sql, params.toArray()};
    }
}
