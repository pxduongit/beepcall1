/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.dao;

import com.cms.xsonline.base.BaseDAO;
import com.cms.xsonline.dto.*;
import com.cms.xsonline.util.Constant;
import com.cms.xsonline.util.Utilities;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author pxduong
 */
@Repository
public class BranchDAO extends BaseDAO {

    //    @Autowired
//    private DataSource dataSource;
//
//    @PostConstruct
//    private void initialisze() {
//        setDataSource(dataSource);
//    }
    public List<BranchDTO> getAllBranch() {
        List<BranchDTO> result = new ArrayList<>();
        StringBuilder queryString = new StringBuilder();
        queryString.append(" SELECT * FROM D_AGENTS WHERE IS_ACTIVE = 1 ");
        SqlRowSet dataRow = null;
        try {
            dataRow = getJdbcTemplate().queryForRowSet(queryString.toString(), new Object[]{});

        } catch (Exception ex) {

        }
        result = extractBranch(dataRow);
        return result;
    }

    public Integer getCount(BranchQueryDTO query) {
        Object[] strQuery = getStrQuery(1, query);
        if (strQuery != null) {
            String queryString = strQuery[0].toString();
            Object[] params = (Object[]) strQuery[1];
            SqlRowSet dataRow = null;
            try {
                dataRow = getJdbcTemplate().queryForRowSet(queryString, params);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
            if (dataRow != null && dataRow.next()) {
                return dataRow.getInt("COUNT");
            }
        }
        return 0;
    }

    public List<BranchDTO> getBranch(BranchQueryDTO query) {
        List<BranchDTO> result = new ArrayList<>();
        Object[] strQuery = getStrQuery(0, query);
        if (strQuery != null) {
            String queryString = strQuery[0].toString();
            Object[] params = (Object[]) strQuery[1];
            SqlRowSet dataRow = null;
            try {
                dataRow = getJdbcTemplate().queryForRowSet(queryString, params);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
            result = extractBranch(dataRow);
        }
        return result;
    }

    public List<BranchDTO> getBranchById(Long id) {
        List<BranchDTO> result = new ArrayList<>();
        StringBuilder sql = new StringBuilder();
        List<Object> params = new ArrayList<>();
        sql.append("SELECT * FROM D_AGENTS WHERE AGENT_ID = ?");
        params.add(id);
        SqlRowSet dataRow = null;
        try {
            dataRow = getJdbcTemplate().queryForRowSet(sql.toString(), params.toArray());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        result = extractBranch(dataRow);
        return result;
    }

    public Integer getCountBranchDetail(BranchDetailQueryDTO query) {
        Object[] strQuery = getBranchDetailStrQuery(1, query);
        if (strQuery != null) {
            String queryString = strQuery[0].toString();
            Object[] params = (Object[]) strQuery[1];
            SqlRowSet dataRow = null;
            try {
                dataRow = getJdbcTemplate().queryForRowSet(queryString, params);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
            if (dataRow != null && dataRow.next()) {
                return dataRow.getInt("COUNT");
            }
        }
        return 0;
    }

    public List<ContactDTO> getBranchDetail(BranchDetailQueryDTO query) {
        List<ContactDTO> result = new ArrayList<>();
        Object[] strQuery = getBranchDetailStrQuery(0, query);
        if (strQuery != null) {
            String queryString = strQuery[0].toString();
            Object[] params = (Object[]) strQuery[1];
            SqlRowSet dataRow = null;
            try {
                dataRow = getJdbcTemplate().queryForRowSet(queryString, params);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
            result = extractBranchDetail(dataRow);
        }
        return result;
    }

    public int createBranch(BranchDTO branch) {
        try {
            StringBuilder queryString = new StringBuilder();

            queryString.append("INSERT INTO D_AGENTS"
                    + "(AGENT_ID, AGENT_CODE, AGENT_NAME, ADDR, IS_ACTIVE, DISCOUNT) "
                    + "VALUES (D_AGENTS_SEQ.nextval, ?, ?, ?, ?, ?)");

            int result = getJdbcTemplate().update(queryString.toString(), new Object[]{
                branch.getCode(),
                branch.getName(),
                branch.getAddr(),
                1,
                branch.getDiscount()
            });
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    public int createBranch(BranchDTO branch, Long id) {
        try {
            StringBuilder queryString = new StringBuilder();

            queryString.append("INSERT INTO D_AGENTS"
                    + "(AGENT_ID, AGENT_CODE, AGENT_NAME, ADDR, IS_ACTIVE, DISCOUNT) "
                    + "VALUES (?, ?, ?, ?, ?, ?)");

            int result = getJdbcTemplate().update(queryString.toString(), new Object[]{
                id,
                branch.getCode(),
                branch.getName(),
                branch.getAddr(),
                1,
                branch.getDiscount()
            });
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    public int createQueues(QueuesDTO queue) {
        try {
            StringBuilder queryString = new StringBuilder();

            queryString.append("INSERT INTO QUEUES"
                    + "(QUEUE_ID, DESCRIPTION, QUEUE_MANAGER_ID, SERVICE_ID, TENANT_ID) "
                    + "VALUES (QUEUES_SEQ.nextval, ?, ?, ?, ?)");

            int result = getJdbcTemplate().update(queryString.toString(), new Object[]{
                queue.getDescription(),
                queue.getQueueManagerId(),
                queue.getServiceId(),
                queue.getTenanIdl()
            });
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    public int createQueues(QueuesDTO queue, Long id) {
        try {
            StringBuilder queryString = new StringBuilder();

            queryString.append("INSERT INTO QUEUES"
                    + "(QUEUE_ID, DESCRIPTION, QUEUE_MANAGER_ID, SERVICE_ID, TENANT_ID) "
                    + "VALUES (?, ?, ?, ?, ?)");

            int result = getJdbcTemplate().update(queryString.toString(), new Object[]{
                id,
                queue.getDescription(),
                queue.getQueueManagerId(),
                queue.getServiceId(),
                queue.getTenanIdl()
            });
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    public int createQueuesParam(QueueParameterDTO param) {
        try {
            StringBuilder queryString = new StringBuilder();

            queryString.append("INSERT INTO QUEUE_PARAM"
                    + "(QUEUE_ID, PARAM_ID, DESCRIPTION, VALUE) "
                    + "VALUES (?, ?, ?, ?)");

            int result = getJdbcTemplate().update(queryString.toString(), new Object[]{
                param.getQueueId(),
                param.getParamId(),
                param.getDescription(),
                param.getValue()
            });
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    public int createQueuesAgent(QueueAgentDTO queueAgent) {
        try {
            StringBuilder queryString = new StringBuilder();

            queryString.append("INSERT INTO QUEUE_AGENT"
                    + "(QUEUE_ID, AGENT_ID, PRIORITY, SKILL_LEVEL, LEVEL_PRIORITY) "
                    + "VALUES (?, ?, ?, ?, ?)");

            int result = getJdbcTemplate().update(queryString.toString(), new Object[]{
                queueAgent.getQueueId(),
                queueAgent.getAgentId(),
                queueAgent.getPriority(),
                queueAgent.getSkillLevel(),
                queueAgent.getLevelPriority()
            });
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

        return 0;
    }
    
    //agentId tuong duong so dien thoai admin, queueId tuong duong id cua toa nha
    public Integer deleteQueueAgent(String agentId, Long queueId) {
        StringBuilder sql = new StringBuilder();
        List<Object> params = new ArrayList<>();
        sql.append("DELETE FROM QUEUE_AGENT WHERE QUEUE_ID = ? AND AGENT_ID = ?");
        params.add(queueId);
        params.add(agentId);
        int result = getJdbcTemplate().update(sql.toString(), params.toArray());
        return result;
    }

    public int addContact(ContactDTO contact) {
        try {
            StringBuilder queryString = new StringBuilder();
            queryString.append("INSERT INTO D_CONTACT"
                    + "(CONTACT_ID, MSISDN, AGENT_ID, IS_ACTIVE, CONTACT_NAME, DESCRIPTION, CREATED_DATE, CREATED_BY, USER_ID) "
                    + "VALUES (D_CONTACT_SEQ.nextval, ?, ?, ?, ?, ?, SYSDATE, ?, ?)");

            int result = getJdbcTemplate().update(queryString.toString(), new Object[]{
                contact.getMsisdn(),
                contact.getAgentId(),
                contact.getIsActive(),
                contact.getContactName(),
                contact.getDescription(),
                contact.getCreatedBy(),
                contact.getUserId()
            });
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    public int changeContactStatus(Long id, String status) {
        try {
            StringBuilder queryString = new StringBuilder();
            queryString.append("UPDATE D_CONTACT SET IS_ACTIVE = ? WHERE CONTACT_ID = ?");
            int result = getJdbcTemplate().update(queryString.toString(), new Object[]{
                status,
                id
            });
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    public int editBranch(BranchDTO branch) {
        try {
            StringBuilder queryString = new StringBuilder();
            List<Object> params = new ArrayList<>();
            queryString.append("UPDATE D_AGENTS SET UPDATED_DATE = TO_DATE(?,'").append(Constant.sqlDateTimeFormat).append("') ");
            params.add(Utilities.getDateStr(new Date()));
            queryString.append(", AGENT_NAME = ?");
            params.add(branch.getName());
            queryString.append(", ADDR = ?");
            params.add(branch.getAddr());
            queryString.append(", DISCOUNT = ?");
            params.add(branch.getDiscount());
            queryString.append(" WHERE AGENT_ID = ?");
            params.add(branch.getId());
            int result = getJdbcTemplate().update(queryString.toString(), params.toArray());
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }

    public Integer deleteBranch(Long id) {
        StringBuilder sql = new StringBuilder();
        List<Object> params = new ArrayList<>();

        sql.append("UPDATE D_AGENTS SET IS_ACTIVE=0, UPDATED_DATE = TO_DATE(?,'").append(Constant.sqlDateTimeFormat).append("') WHERE AGENT_ID = ?");
        params.add(Utilities.getDateStr(new Date()));
        params.add(id);
        int result = getJdbcTemplate().update(sql.toString(), params.toArray());
        return result;
    }

    public Integer deleteContact(Long id) {
        StringBuilder sql = new StringBuilder();
        List<Object> params = new ArrayList<>();

        sql.append("DELETE FROM D_CONTACT WHERE CONTACT_ID = ?");
        params.add(id);
        int result = getJdbcTemplate().update(sql.toString(), params.toArray());
        return result;
    }

    private Object[] getStrQuery(int type, BranchQueryDTO query) {
        StringBuilder sql = new StringBuilder();
        List<Object> params = new ArrayList<>();
        if (type == 1) {//count
            sql.append(" SELECT COUNT(*) COUNT FROM (");
        } else if (type == 0) {//select *
            sql.append("SELECT * FROM (SELECT ROWNUM RNUM, A.*  FROM (");
        } else {
            return null;
        }

        sql.append("SELECT * FROM D_AGENTS WHERE IS_ACTIVE=1 AND 1=1 ");
        if (query.getBranchId() != null) {
            sql.append(" AND AGENT_ID = ?");
            params.add(query.getBranchId());
        }
        if (query.getBranchCode() != null && query.getBranchCode() != "") {
            sql.append(" AND LOWER(AGENT_CODE) like ?");
            params.add("%" + query.getBranchCode().toLowerCase() + "%");
        }
        if (query.getBranchName() != null && query.getBranchName() != "") {
            sql.append(" AND LOWER(AGENT_NAME) like ?");
            params.add("%" + query.getBranchName().toLowerCase() + "%");
        }
        if (type == 0) {//chi co lay thong tin moi can loc theo so trang
            if (query.getCurPage() != null && query.getCurPage() > 0 && query.getPageSize() != null && query.getPageSize() > 0) {
                int from = (query.getPageSize() * (query.getCurPage() - 1)) + 1;
                int to = from + query.getPageSize() - 1;
                sql.append(") A WHERE ROWNUM<=?) WHERE RNUM>=?");
                params.add(to);
                params.add(from);
            } else {
                sql.append(") A)");
            }
        } else {
            sql.append(")");
        }
        return new Object[]{sql, params.toArray()};
    }

    private Object[] getBranchDetailStrQuery(int type, BranchDetailQueryDTO query) {
        StringBuilder sql = new StringBuilder();
        List<Object> params = new ArrayList<>();
        if (type == 1) {//count
            sql.append(" SELECT COUNT(*) COUNT FROM (");
        } else if (type == 0) {//select *
            sql.append("SELECT * FROM (SELECT ROWNUM RNUM, A.*  FROM (");
        } else {
            return null;
        }

        sql.append("SELECT * FROM D_CONTACT WHERE 1=1 ");
        if (query.getObjId() != null) {
            sql.append(" AND AGENT_ID = ?");
            params.add(query.getObjId());
        }
        if (query.getIsActive() != null) {
            sql.append(" AND IS_ACTIVE = ?");
            params.add(query.getIsActive());
        }
        if (query.getPhoneNumberExclude() != null) {
            sql.append(" AND MSISDN <> ?");
            params.add(query.getPhoneNumberExclude());
        }
        if (query.getPhoneNumber() != null) {
            sql.append(" AND LOWER(MSISDN) LIKE ?");
            params.add("%" + query.getPhoneNumber().toLowerCase() + "%");
        }
        if (query.getName() != null) {
            sql.append(" AND LOWER(CONTACT_NAME) LIKE ?");
            params.add("%" + query.getName().toLowerCase() + "%");
        }
        if (query.getDescription() != null) {
            sql.append(" AND LOWER(DESCRIPTION) LIKE ?");
            params.add("%" + query.getDescription().toLowerCase() + "%");
        }
        sql.append(" ORDER BY CREATED_DATE DESC");

        if (type == 0) {//chi co lay thong tin moi can loc theo so trang
            if (query.getCurPage() != null && query.getCurPage() > 0 && query.getPageSize() != null && query.getPageSize() > 0) {
                int from = (query.getPageSize() * (query.getCurPage() - 1)) + 1;
                int to = from + query.getPageSize() - 1;
                sql.append(") A WHERE ROWNUM<=?) WHERE RNUM>=?");
                params.add(to);
                params.add(from);
            } else {
                sql.append(") A)");
            }
        } else {
            sql.append(")");
        }
        return new Object[]{sql, params.toArray()};
    }

    public ContactDTO getContactById(Long id) {
        String queryString = "SELECT * FROM D_CONTACT WHERE CONTACT_ID = ?";
        Object[] params = new Object[]{id};
        SqlRowSet dataRow = null;
        try {
            dataRow = getJdbcTemplate().queryForRowSet(queryString, params);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        List<ContactDTO> result = extractBranchDetail(dataRow);
        return result.get(0);

    }

    public ContactDTO getContactByUserId(Long id) {
        String queryString = "SELECT * FROM D_CONTACT WHERE USER_ID = ?";
        Object[] params = new Object[]{id};
        SqlRowSet dataRow = null;
        try {
            dataRow = getJdbcTemplate().queryForRowSet(queryString, params);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        List<ContactDTO> result = extractBranchDetail(dataRow);
        if (result.size() > 0) {
            return result.get(0);
        } else {
            return null;
        }
    }

    private Object[] getStrQueryCallInfo(int type, CallInfoQueryDTO query) {
        StringBuilder sql = new StringBuilder();
        List<Object> params = new ArrayList<>();
        if (type == 1) {//count
            sql.append(" SELECT COUNT(*) COUNT FROM (");
        } else if (type == 0) {//select *
            sql.append("SELECT * FROM (SELECT ROWNUM RNUM, A.*  FROM (");
        } else {
            return null;
        }

        sql.append("SELECT * FROM CALLINFO CI "
                + " JOIN D_AGENTS A on A.AGENT_ID = CI.QUEUE_ID"
                + " WHERE 1=1 ");
        //branch Id
        if (query.getBranchId() != null) {
            sql.append(" AND A.AGENT_ID = ?");
            params.add(query.getBranchId());
        }
//phone number
        if (query.getAgentId() != null) {
            sql.append(" AND CI.AGENT_ID = ?");
            params.add(query.getAgentId());
        }

        if (query.getCaller() != null) {
            sql.append(" AND LOWER(CI.CALLER) LIKE ?");
            params.add("%" + query.getCaller().toLowerCase() + "%");
        }
        if (query.getPackageId() != null) {
            sql.append(" AND CI.PACKAGE_ID = ?");
            params.add(query.getPackageId());
        }
        if (query.getFromDate() != null) {
            sql.append(" AND CI.START_TIME >= TO_DATE(?,'").append(Constant.sqlDateTimeFormat).append("') ");
            params.add(Utilities.getDateStr(query.getFromDate()));
        }
        if (query.getToDate() != null) {
            sql.append(" AND CI.START_TIME <= TO_DATE(?,'").append(Constant.sqlDateTimeFormat).append("') ");
            params.add(Utilities.getDateStr(query.getToDate()));
        }
        sql.append(" ORDER BY CI.START_TIME DESC ");

        if (type == 0) {//chi co lay thong tin moi can loc theo so trang
            if (query.getCurPage() != null && query.getCurPage() > 0 && query.getPageSize() != null && query.getPageSize() > 0) {
                int from = (query.getPageSize() * (query.getCurPage() - 1)) + 1;
                int to = from + query.getPageSize() - 1;
                sql.append(") A WHERE ROWNUM<=?) WHERE RNUM>=?");
                params.add(to);
                params.add(from);
            } else {
                sql.append(") A)");
            }
        } else {
            sql.append(")");
        }
        return new Object[]{sql, params.toArray()};
    }

    private List<BranchDTO> extractBranch(SqlRowSet dataRow) {
        List<BranchDTO> result = new ArrayList<>();
        while (dataRow != null && dataRow.next()) {
            BranchDTO temp = new BranchDTO();
            temp.setId(dataRow.getLong("AGENT_ID"));
            temp.setCode(dataRow.getString("AGENT_CODE"));
            temp.setName(dataRow.getString("AGENT_NAME"));
            temp.setAddr(dataRow.getString("ADDR"));
            temp.setDiscount(dataRow.getFloat("DISCOUNT"));
            result.add(temp);
        }
        return result;
    }

    private List<ContactDTO> extractBranchDetail(SqlRowSet dataRow) {
        List<ContactDTO> result = new ArrayList<>();
        while (dataRow != null && dataRow.next()) {
            ContactDTO temp = new ContactDTO();
            temp.setAgentId(dataRow.getLong("AGENT_ID"));
            temp.setContactId(dataRow.getLong("CONTACT_ID"));
            temp.setContactName(dataRow.getString("CONTACT_NAME"));
            temp.setDescription(dataRow.getString("DESCRIPTION"));
            temp.setIsActive(dataRow.getString("IS_ACTIVE"));
            temp.setMsisdn(dataRow.getString("MSISDN"));
            temp.setUserId(dataRow.getLong("USER_ID"));
            result.add(temp);
        }
        return result;
    }

    private List<CallInfoDTO> extractCallInfo(SqlRowSet dataRow) {
        List<CallInfoDTO> result = new ArrayList<>();
        while (dataRow != null && dataRow.next()) {
            CallInfoDTO temp = new CallInfoDTO();
            temp.setAgentName(dataRow.getString("AGENT_NAME"));
            temp.setQueueId(dataRow.getString("QUEUE_ID"));
            temp.setCallId(dataRow.getString("CALL_ID"));
            temp.setCaller(dataRow.getString("CALLER"));
            temp.setCalled(dataRow.getString("CAllED"));
            temp.setAgentId(dataRow.getString("AGENT_ID"));
            temp.setStartTime(dataRow.getTimestamp("START_TIME"));
            temp.setFirstConnectTime(dataRow.getTimestamp("FIRST_CONNECT_TIME"));
            temp.setAcdTime(dataRow.getLong("ACD_TIME"));
            temp.setUrl(dataRow.getString("URL"));
            result.add(temp);
        }
        return result;
    }

    public Integer CheckAgentCodeExists(BranchDTO agentInfo) {
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT COUNT(*) COUNT FROM D_AGENTS WHERE LOWER(AGENT_CODE) = ?");
        SqlRowSet dataRow = null;
        try {
            dataRow = getJdbcTemplate().queryForRowSet(sql.toString(), new Object[]{agentInfo.getCode().toLowerCase()});
        } catch (Exception ex) {
        }
        if (dataRow != null && dataRow.next()) {
            return dataRow.getInt("COUNT");
        }
        return 0;
    }

    public void saveContactBatch(final Long batchId, final List<ContactDTO> listContact) {

        StringBuilder queryString = new StringBuilder();
        queryString.append("INSERT INTO D_CONTACT (CONTACT_ID, MSISDN, AGENT_ID, IS_ACTIVE, CONTACT_NAME"
                + ", DESCRIPTION, CREATED_DATE, CREATED_BY, USER_ID) "
                + "VALUES (D_CONTACT_SEQ.nextval, ?, ?, ?, ?, ?, SYSDATE, ?, ?)");
        String QUERY_SAVE = queryString.toString();
        getJdbcTemplate().batchUpdate(QUERY_SAVE,
                new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement ps, int i)
                    throws SQLException {
                ContactDTO bo = listContact.get(i);
                ps.setString(1, bo.getMsisdn());
                ps.setLong(2, bo.getAgentId());
                ps.setString(3, bo.getIsActive());
                ps.setString(4, bo.getContactName() == null ? "" : bo.getContactName());
                ps.setString(5, bo.getDescription() == null ? "" : bo.getDescription());
                ps.setLong(6, bo.getCreatedBy() == null ? 0l : bo.getCreatedBy());
                ps.setLong(7, bo.getUserId()== null ? 0l : bo.getUserId());
            }

            @Override
            public int getBatchSize() {
                return listContact.size();
            }

        });

    }

    public Long getAgentsSeqNextval() {
        String queryString = "select D_AGENTS_seq.nextval id from dual";
        try {
            SqlRowSet dataRow = getJdbcTemplate().queryForRowSet(queryString);
            if (dataRow != null && dataRow.next()) {
                return dataRow.getLong("id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0l;
    }

    public Long getQueuesSeqNextval() {
        String queryString = "select QUEUES_SEQ.nextval id from dual";
        try {
            SqlRowSet dataRow = getJdbcTemplate().queryForRowSet(queryString);
            if (dataRow != null && dataRow.next()) {
                return dataRow.getLong("id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0l;
    }

    public Long getAgentsCodeSeqNextval() {
        String queryString = "select D_AGENT_CODE_SEQ.nextval id from dual";
        try {
            SqlRowSet dataRow = getJdbcTemplate().queryForRowSet(queryString);
            if (dataRow != null && dataRow.next()) {
                return dataRow.getLong("id");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0l;
    }

    public Integer getCountCallInfo(CallInfoQueryDTO query) {
        Object[] strQuery = getStrQueryCallInfo(1, query);
        if (strQuery != null) {
            String queryString = strQuery[0].toString();
            Object[] params = (Object[]) strQuery[1];
            SqlRowSet dataRow = null;
            try {
                dataRow = getJdbcTemplate().queryForRowSet(queryString, params);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
            if (dataRow != null && dataRow.next()) {
                return dataRow.getInt("COUNT");
            }
        }
        return 0;
    }

    public List<CallInfoDTO> getCallInfo(CallInfoQueryDTO query) {
        List<CallInfoDTO> result = new ArrayList<>();
        Object[] strQuery = getStrQueryCallInfo(0, query);
        if (strQuery != null) {
            String queryString = strQuery[0].toString();
            Object[] params = (Object[]) strQuery[1];
            SqlRowSet dataRow = null;
            try {
                dataRow = getJdbcTemplate().queryForRowSet(queryString, params);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
            result = extractCallInfo(dataRow);
        }
        return result;
    }
    
    public int updateContactPhone(Long id, String phone) {
        try {
            StringBuilder queryString = new StringBuilder();
            queryString.append("UPDATE D_CONTACT SET MSISDN = ? WHERE CONTACT_ID = ?");
            int result = getJdbcTemplate().update(queryString.toString(), new Object[]{
                phone,
                id
            });
            return result;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return 0;
    }
}
