package com.cms.xsonline.obj.exception;

public class FAException extends Exception {
    private int errorCode = 404;

    public FAException(int code) {
        super();
        errorCode = code;
    }

    public FAException(String message) {
        super(message);
    }

    public FAException(int code, String message) {
        super(message);
        errorCode = code;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }
}
