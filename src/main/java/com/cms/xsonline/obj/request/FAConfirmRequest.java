package com.cms.xsonline.obj.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FAConfirmRequest {
    @JsonProperty(value = "building_id")
    public int buildingId;

    public String note;

    public int getBuildingId() {
        return buildingId;
    }

    public void setBuildingId(int buildingId) {
        this.buildingId = buildingId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
