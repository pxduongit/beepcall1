package com.cms.xsonline.obj.request;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonAutoDetect
@JsonIgnoreProperties
public class FAUpdateRequest {

    @JsonProperty(value = "first_name")
    public String firstName;

    @JsonProperty(value = "last_name")
    public String lastName;

    @JsonProperty(value = "phone")
    public String phone;

    @JsonProperty(value = "email")
    public String email;
}
