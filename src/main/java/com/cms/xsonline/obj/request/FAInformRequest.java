package com.cms.xsonline.obj.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FAInformRequest {

    @JsonProperty(value = "building_code")
    public String buildingCode;

    @JsonProperty(value = "building_id")
    public long buildingId;

    @JsonProperty(value = "floor_id")
    public int floorId = -1;

    @JsonProperty(value = "note")
    public String note;

}
