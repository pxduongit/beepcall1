package com.cms.xsonline.obj.response;

import com.cms.xsonline.util.Constant;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FAGenericResponse<T> {

    @JsonProperty(value = "response_code")
    public int responseCode = Constant.RESPONSE_CODE_OK;

    public String code = "";

    public T data;

}
