/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.controller;

import com.cms.xsonline.base.BaseController;
import com.cms.xsonline.dto.AccountDTO;
import com.cms.xsonline.dto.DataTableDTO;
import org.springframework.stereotype.Controller;
import com.cms.xsonline.service.CallOutEventService;
import java.security.Principal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author duongpx2
 */
@Controller
public class CallOutEventController extends BaseController {

    private CallOutEventService callOutEventService;

    @Autowired(required = true)
    @Qualifier(value = "callOutEventService")
    public void setPersonService(CallOutEventService bs) {
        this.callOutEventService = bs;
    }

    @RequestMapping(value = "/callOur/makeCallOut", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public DataTableDTO getAccountList(
            //            @RequestParam(required = false) String agentId,
            @RequestBody DataTableDTO agentId,
            ModelMap model, Principal principal) {
        DataTableDTO response = new DataTableDTO();
        response.setPageSize(99999);
        AccountDTO accountInfo = (AccountDTO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return response;
    }

}
