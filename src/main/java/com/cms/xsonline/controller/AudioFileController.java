/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.controller;

import com.cms.xsonline.base.BaseController;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author asus
 */
@Controller
@RequestMapping(value = "/audio")
public class AudioFileController extends BaseController {

//    private static final String FILE_PATH = "//u01//3csoft//audioconverter//data//wav";
    private static final String FILE_PATH = "C:\\Users\\Public\\Music\\Sample Music";
//    private static final String APPLICATION_PDF = "application/pdf";

    @RequestMapping(value = "/getfile", method = RequestMethod.GET, 
            produces = { MediaType.APPLICATION_OCTET_STREAM_VALUE })
    public @ResponseBody
    void getfile(
            @RequestParam(required = false) String f,
            HttpServletResponse response) throws IOException {
        File file = getFile(f);
        InputStream in = new FileInputStream(file);
        response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
        response.setHeader("Content-Disposition", "attachment; filename=" + file.getName());
        response.setHeader("Content-Length", String.valueOf(file.length()));
        FileCopyUtils.copy(in, response.getOutputStream());
    }

    private File getFile(String fileName) throws FileNotFoundException {
        File file = new File(FILE_PATH + "\\" + fileName);
        if (!file.exists()) {
            throw new FileNotFoundException("file with path: " + FILE_PATH + " was not found.");
        }
        return file;
    }
}
