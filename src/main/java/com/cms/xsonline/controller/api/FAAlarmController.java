package com.cms.xsonline.controller.api;


import com.cms.xsonline.facade.AlarmFacade;
import com.cms.xsonline.obj.exception.FAException;
import com.cms.xsonline.obj.request.FAConfirmRequest;
import com.cms.xsonline.obj.request.FAInformRequest;
import com.cms.xsonline.obj.response.FAGenericResponse;
import com.cms.xsonline.util.Constant;
import com.cms.xsonline.util.TokenUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api")
public class FAAlarmController {

    private final Logger logger = LoggerFactory.getLogger(FAAlarmController.class);

    @Autowired
    private AlarmFacade alarmFacade;


    @RequestMapping(value = "/v1/alert", method = RequestMethod.POST)
    public @ResponseBody
    FAGenericResponse<Integer> informFireTrouble(@RequestHeader("X-Token") String token,
                                                 @RequestBody() FAInformRequest informRequest) {

        logger.info("informFireTrouble }===============>>>>");
        FAGenericResponse<Integer> response = new FAGenericResponse<>();
        // validate token
        if (TokenUtils.isTokenExpired(token)) {
            response.responseCode = Constant.ERROR_CODE_TOKEN_EXPIRED;
            response.code = "Token is expired or invalid";
            return response;
        }
        try {
            int currentUser = TokenUtils.getUserIdFromToken(token);

            int result = alarmFacade.makeCall(informRequest, currentUser);

            response.responseCode = (result == 1) ? Constant.RESPONSE_CODE_OK : Constant.RESPONSE_CODE_FAILED;
            response.data = result;
            response.code = (result == 1) ? "Sent notification successfully" : "Sent notification failed";
        } catch (FAException e) {
            e.printStackTrace();
            response.code = e.getMessage();
            response.responseCode = e.getErrorCode();
        }
        return response;
    }

    @RequestMapping(value = "/v1/setting/notify", method = RequestMethod.POST)
    public @ResponseBody
    FAGenericResponse<Integer> settingNotify(@RequestHeader("X-Token") String token,
                                             @RequestParam("enabled") boolean enableOrNot) {

        logger.info("settingNotify }===============>>>>");
        FAGenericResponse<Integer> response = new FAGenericResponse<>();
        // validate token
        if (TokenUtils.isTokenExpired(token)) {
            response.responseCode = Constant.ERROR_CODE_TOKEN_EXPIRED;
            response.code = "Token is expired or invalid";
            return response;
        }

        try {
            int userId = TokenUtils.getUserIdFromToken(token);
            int result = alarmFacade.settingAlarm(userId, enableOrNot);
            response.code = String.format("%s notification feature", enableOrNot ? "Enabled" : "Disabled");
            response.responseCode = Constant.RESPONSE_CODE_OK;
            response.data = result;
        } catch (FAException e) {
            e.printStackTrace();
            response.responseCode = Constant.RESPONSE_CODE_FAILED;
            response.code = e.getMessage();
        }

        return response;
    }

    @RequestMapping(value = "/v1/confirm", method = RequestMethod.POST)
    public @ResponseBody
    FAGenericResponse<Integer> confirmFireTrouble(@RequestHeader("X-Token") String token, @RequestBody() FAConfirmRequest informRequest) {

        logger.info("confirmFireTrouble }===============>>>>");
        FAGenericResponse<Integer> response = new FAGenericResponse<>();
        // validate token
        if (TokenUtils.isTokenExpired(token)) {
            response.responseCode = Constant.ERROR_CODE_TOKEN_EXPIRED;
            response.code = "Token is expired or invalid";
            return response;
        }
        //TODO : call facade to get total building management to notify
        //1: If normal user -> need admin/building admin approval to notify
        //2: Building user -> notify immediate ly
        // doing same makeCall at the moemen
        return response;
    }

}
