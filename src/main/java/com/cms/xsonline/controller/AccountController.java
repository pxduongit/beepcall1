/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.controller;

import com.cms.xsonline.base.BaseController;
import com.cms.xsonline.dto.AccountDTO;
import com.cms.xsonline.dto.AccountQueryDTO;
import com.cms.xsonline.dto.BranchDTO;
import com.cms.xsonline.dto.ChangePasswordDTO;
import com.cms.xsonline.dto.DataTableDTO;
import com.cms.xsonline.dto.RoleDTO;
import com.cms.xsonline.dto.UpdateSADiscount;
import com.cms.xsonline.obj.BaseResult;
import com.cms.xsonline.service.AccountService;
import java.security.Principal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.annotation.Resource;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author pxduong
 */
@Controller
public class AccountController extends BaseController {

    @Resource
    private MessageSource messageSource;

    private AccountService accountService;

    @Autowired(required = true)
    @Qualifier(value = "accountService")
    public void setPersonService(AccountService as) {
        this.accountService = as;
    }

    @RequestMapping(value = {"/account/getAccount"}, method = {org.springframework.web.bind.annotation.RequestMethod.GET})
    public String getAccount(
            @RequestParam(required = false) Integer curPage,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) String username,
            @RequestParam(required = false) String phoneNumber,
            @RequestParam(required = false) String agentCode,
            @RequestParam(required = false) String agentName,
            @RequestParam(required = false) Long agentId,
            //            @RequestParam(required = false) Date fromDate,
            //            @RequestParam(required = false) Date toDate,
            ModelMap model, Principal principal) {

        List<AccountDTO> result = new ArrayList<>();
        Integer totalRecord = 0;
        AccountDTO accountInfo = new AccountDTO();
        Map<String, String> listBranchResponse = new HashMap<>();
        try {
            AccountQueryDTO toSearch = new AccountQueryDTO();
            toSearch.setUsername(username);
            toSearch.setPhoneNumber(phoneNumber);
            toSearch.setAgentCode(agentCode);
            toSearch.setAgentName(agentName);
            toSearch.setAgentId(agentId);
            //            toSearch.setFromDate(fromDate);
//            toSearch.setToDate(toDate);
            totalRecord = this.accountService.getCount(toSearch);
            if (pageSize == null) {
                pageSize = 15;
            }
            if (curPage == null || curPage < 1) {
                curPage = 1;
            } else if ((totalRecord / pageSize) + 1 < curPage) {
                curPage = (totalRecord / pageSize) + 1;
            }
            toSearch.setCurPage(curPage);
            toSearch.setPageSize(pageSize);
            result = this.accountService.getByPage(toSearch);
            accountInfo = (AccountDTO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            //init branch search
            List<BranchDTO> listBranch = this.accountService.getAllBranch();
            for (BranchDTO temp : listBranch) {
                listBranchResponse.put(String.valueOf(temp.getId()), temp.getName());
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        AccountQueryDTO toSearchForm = new AccountQueryDTO();
        model.addAttribute("curPage", curPage);
        model.addAttribute("pageSize", pageSize);
        model.addAttribute("totalRecord", totalRecord);
        model.addAttribute("listAccount", result);
        model.addAttribute("toSearchForm", toSearchForm);
        model.addAttribute("accountId", accountInfo.getId());
        model.addAttribute("listBranch", listBranchResponse);
        return "accountManager";
    }

    @RequestMapping(value = {"/account/getAccountList"}, method = {org.springframework.web.bind.annotation.RequestMethod.GET})
    @ResponseBody
    public DataTableDTO getAccountList(
            @RequestParam(required = false) Integer curPage,
            @RequestParam(required = false) Integer pageSize,
            @RequestParam(required = false) String username,
            @RequestParam(required = false) String phoneNumber,
            @RequestParam(required = false) String agentCode,
            @RequestParam(required = false) String agentName,
            @RequestParam(required = false) Long agentId,
            //            @RequestParam(required = false) Date fromDate,
            //            @RequestParam(required = false) Date toDate,
            ModelMap model, Principal principal) {
        //lay den cuoi ngay duoc chon
//        toDate = Utilities.getEndOfDay(toDate);
        DataTableDTO response = new DataTableDTO();
        List<AccountDTO> result = new ArrayList<>();
        Integer totalRecord = 0;
        try {
            AccountQueryDTO toSearch = new AccountQueryDTO();
            toSearch.setUsername(username);
            toSearch.setPhoneNumber(phoneNumber);
            toSearch.setAgentCode(agentCode);
            toSearch.setAgentName(agentName);
            toSearch.setAgentId(agentId);
//            toSearch.setFromDate(fromDate);
//            toSearch.setToDate(toDate);
            totalRecord = this.accountService.getCount(toSearch);
            if (pageSize == null) {
                pageSize = 15;
            }
            if (curPage == null || curPage < 1) {
                curPage = 1;
            } else if ((totalRecord / pageSize) + 1 < curPage) {
                curPage = (totalRecord / pageSize) + 1;
            }
            toSearch.setCurPage(curPage);
            toSearch.setPageSize(pageSize);
            result = this.accountService.getByPage(toSearch);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        response.setCurPage(curPage);
        response.setPageSize(pageSize);
        response.setTotalRecord((totalRecord));
        response.setListData(result);
        return response;
    }

    @RequestMapping(value = {"/account/getAddNewAccount"}, method = {org.springframework.web.bind.annotation.RequestMethod.GET})
    public String addNewAccount(ModelMap model
    ) {
        model.addAttribute("typeView", "new");
        AccountDTO newAccount = new AccountDTO();
        Map<String, String> listBranchResponse = new HashMap<>();
        List<BranchDTO> listBranch = this.accountService.getAllBranch();
        for (BranchDTO temp : listBranch) {
            listBranchResponse.put(String.valueOf(temp.getId()), temp.getName());
        }
        Map<String, String> listRoleResponse = new HashMap<>();
        List<RoleDTO> listRole = this.accountService.getAllRole();
        for (RoleDTO temp : listRole) {
            listRoleResponse.put(String.valueOf(temp.getRoleId()), temp.getRoleName());
        }
        model.addAttribute("account", newAccount);
        model.addAttribute("listBranch", listBranchResponse);
        model.addAttribute("listRole", listRoleResponse);
        return "accountInfo";
    }

    @RequestMapping(value = {"/account/getEditAccount/{id}"}, method = {org.springframework.web.bind.annotation.RequestMethod.GET})
    public String editAccount(@PathVariable("id") Long id, ModelMap model
    ) {
        AccountDTO listAccount = this.accountService.getById(id);
        model.addAttribute("typeView", "edit");
        AccountDTO accountTemplate = listAccount;
        accountTemplate.setIsAdminChangePass(false);
        accountTemplate.setPassword(null);
        accountTemplate.setRePassword(null);
        //Set list branch
        Map<String, String> listBranchResponse = new HashMap<>();
        List<BranchDTO> listBranch = this.accountService.getAllBranch();
        for (BranchDTO temp : listBranch) {
            listBranchResponse.put(String.valueOf(temp.getId()), temp.getName());
        }
        model.addAttribute("account", accountTemplate);
        model.addAttribute("listBranch", listBranchResponse);
        return "accountInfo";
    }

    @RequestMapping(value = {"/account/doEditAccount"}, method = {org.springframework.web.bind.annotation.RequestMethod.POST})
    public String submitEditAccountForm(
            @Valid
            @ModelAttribute("account") AccountDTO updated,
            BindingResult bindingResult,
            RedirectAttributes attributes
    ) {
        try {
            BaseResult result = this.accountService.editAccount(updated);
            if (result.isIsFailed()) {
                attributes.addFlashAttribute("errorMessage", result.getErrorMessage());

            } else {
                attributes.addFlashAttribute("feedbackMessage", result.getErrorMessage());
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "redirect:/account/getAccount";
    }

    @RequestMapping(value = {"/account/doAddNewAccount"}, method = {org.springframework.web.bind.annotation.RequestMethod.POST})
    public String submitAddNewAccountForm(@Valid
            @ModelAttribute("account") AccountDTO updated,
            BindingResult bindingResult,
            RedirectAttributes attributes
    ) {
        try {
            BaseResult result = this.accountService.createAccount(updated);
            if (result.isIsFailed()) {
                attributes.addFlashAttribute("errorMessage", result.getErrorMessage());
            } else {
                attributes.addFlashAttribute("feedbackMessage", result.getErrorMessage());
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return "redirect:/account/getAccount";
    }

    @RequestMapping(value = {"/account/doDeleteAccount"}, method = {org.springframework.web.bind.annotation.RequestMethod.GET})
//    @ResponseBody
    public String deleteAccount(
            @Valid
            @RequestParam(required = true) Long id,
            RedirectAttributes attributes
    ) {
        try {
            if (this.accountService.deleteAccount(id) > 0) {
                attributes.addFlashAttribute("feedbackMessage", "Delete account succcess!");
            } else {
                attributes.addFlashAttribute("errorMessage", "Delete account failed. Please try again...");
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return "redirect:/account/getAccount";
    }

    @RequestMapping(value = {"/account/doUnlockAccount"}, method = {org.springframework.web.bind.annotation.RequestMethod.GET})
//    @ResponseBody
    public String unlockAccount(
            @Valid
            @RequestParam(required = true) Long id,
            ModelMap model
    ) {
        Integer result = 0;
        try {
            result = this.accountService.unlockAccount(id);
            if (result > 0) {
                AccountDTO accountInfo = (AccountDTO) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                if (Objects.equals(id, accountInfo.getId())) {
                    //neu account unlock cho chinh no--> cap nhat lai principal
                    accountInfo.setIsLocked(false);
                    Authentication newPrincipal = new UsernamePasswordAuthenticationToken(accountInfo, accountInfo.getPassword(), accountInfo.getListRoles());
                    SecurityContextHolder.getContext().setAuthentication(newPrincipal);
                }

            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return "redirect:/account/getAccount";
    }

    @RequestMapping(value = {"/getChangePass"}, method = {org.springframework.web.bind.annotation.RequestMethod.GET})
    public String getChangePassForm(
            @RequestParam(required = false) String error,
            ModelMap model, Principal principal) {
        try {
            model.addAttribute("error", error);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return "changePass";
    }

    @RequestMapping(value = {"/doChangePass"}, method = {org.springframework.web.bind.annotation.RequestMethod.POST})
    public String changePass(@Valid
            @ModelAttribute("changepassAttr") ChangePasswordDTO toChange,
            BindingResult bindingResult,
            RedirectAttributes attributes
    ) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        AccountDTO accountInfo = (AccountDTO) authentication.getPrincipal();
        try {
            if (this.accountService.changePass(accountInfo, toChange) > 0) {
                attributes.addFlashAttribute("feedbackMessage", "Change password success");
            } else {
//                attributes.addFlashAttribute("errorMessage", "Change password failed!");
                attributes.addAttribute("error", "true");
                return "redirect:/getChangePass";
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return "redirect:/getHome";
    }

    @RequestMapping(value = {"/doUpdateSADiscount"}, method = {org.springframework.web.bind.annotation.RequestMethod.POST})
    public String updateLotteryResult(@Valid
            @ModelAttribute("spAgentDiscount") UpdateSADiscount discount,
            BindingResult bindingResult,
            RedirectAttributes attributes
    ) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        AccountDTO accountInfo = (AccountDTO) authentication.getPrincipal();

        BaseResult result = this.accountService.updateSuperAgentDiscount(accountInfo, discount.getDiscount());
        if (result.isIsFailed()) {
            attributes.addFlashAttribute("errorMessage", result.getErrorMessage());
        } else {
            attributes.addFlashAttribute("feedbackMessage", result.getErrorMessage());
        }
        return "redirect:/account/getAccount";
    }
}
