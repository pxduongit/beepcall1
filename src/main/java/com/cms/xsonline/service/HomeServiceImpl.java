/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.service;

import com.cms.xsonline.dao.CallOutEventDAO;
import com.cms.xsonline.dto.AccountDTO;
import com.cms.xsonline.dto.LotteryQueryDTO;
import com.cms.xsonline.dto.LotteryResultDTO;
import com.cms.xsonline.dto.PlayerDTO;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Service;

/**
 *
 * @author pxduong
 */
@Service("homeService")
public class HomeServiceImpl implements HomeService {

    @Resource
    private CallOutEventDAO lotteryResultDAO;

    @Override
    public Integer getCount(LotteryQueryDTO query) {
        return this.lotteryResultDAO.getCount(query);
    }

    @Override
    public List<LotteryResultDTO> getByPage(LotteryQueryDTO query) {
        return this.lotteryResultDAO.getByPage(query);
    }

    @Override
    public Integer updateResult(AccountDTO accountInfo, LotteryResultDTO result) {
        return this.lotteryResultDAO.insertResult(accountInfo, result);
    }

    @Override
    public Integer updateTotalRow(int totalRow, long id) {
        return this.lotteryResultDAO.updateTotalRow(totalRow, id);
    }

    @Override
    public Integer insertPokeCall(PlayerDTO result) {
        return this.lotteryResultDAO.insertPokeCall(result);
    }

    @Override
    public void saveBatch(final List<PlayerDTO> playerDTOs, Long insertedId) {
        this.lotteryResultDAO.saveBatch(playerDTOs, insertedId);
    }

    @Override
    public Integer commitResult(AccountDTO accountInfo) {
//        String curTime = Utilities.getShortDateStrJS(new Date());
//        LotteryResultDTO checkExists = this.lotteryResultDAO.getIdByDate(curTime);
//        if (checkExists.getId() != null && checkExists.getId() > 0) {
//            if (checkExists.isIsCommited()) {
//                return 201;//da co nguoi commit roi
//            }
//            return this.lotteryResultDAO.CommitResult(accountInfo, checkExists.getId());
//        }
        return 0;
    }

    @Override
    public boolean checkEnableUpdate(LotteryResultDTO result) {
//        if (result.isIsCommited()) {
//            return false;
//        }
        return true;
    }

    @Override
    public boolean checkEnableCommit(LotteryResultDTO result) {
//        if (result.getResultCode() != null && !"".equals(result.getResultCode())
//                && !result.isIsCommited()) {
//            return true;
//        }
        return false;
    }

}
