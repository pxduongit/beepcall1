/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.service;

import com.cms.xsonline.dao.*;
import com.cms.xsonline.dto.*;
import com.cms.xsonline.obj.BaseResult;
import com.cms.xsonline.util.Constant;
import com.cms.xsonline.util.IUtils;
import com.cms.xsonline.util.Utilities;
import com.google.common.base.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * @author pxduong
 */
@Service("branchService")
public class BranchServiceImpl implements BranchService {

    @Resource
    private BranchDAO branchDAO;
    @Resource
    private ContactDAO playerDAO;
    @Resource
    private AccountDAO accountDAO;
    @Resource
    private CallOutDAO callOutDAO;
    @Resource
    private CallOutEventDAO lotteryResultDAO;
    private AccountService accountService;

    @Autowired(required = true)
    @Qualifier(value = "accountService")
    public void setAccountService(AccountService as) {
        this.accountService = as;
    }

    @Override
    public List<BranchDTO> getByPage(BranchQueryDTO query) {
//        System.out.println(Config.getInstance().getTimeCheckMakeCall());
        return this.branchDAO.getBranch(query);
    }

    @Override
    public Integer getCount(BranchQueryDTO query) {
        return this.branchDAO.getCount(query);
    }

    @Override
    public Integer deleteBranch(Long id) {
        int deleteBranchResult = this.branchDAO.deleteBranch(id);
        if (deleteBranchResult > 0) {
            List<AccountDTO> listaccount = this.accountDAO.getByAgentId(id);
            for (AccountDTO account : listaccount) {
                this.accountService.changeActiveStatus(account, 0);
            }
        }
        return deleteBranchResult;
    }

    @Override
    public List<BranchDTO> getById(Long id) {
        return this.branchDAO.getBranchById(id);
    }

    @Override
    public Integer addNewBranch(BranchDTO branch) {
        //gen code toa nha
        String code = "BLD" + this.branchDAO.getAgentsCodeSeqNextval();
        branch.setCode(code);

        //khoi tao toa nha
        Long branchSeqId = this.branchDAO.getAgentsSeqNextval();
        int i = this.branchDAO.createBranch(branch, branchSeqId);
        if (i > 0) {
            //khoi tao queue - id queue trung voi id branch
//            Long queueSeqId = this.branchDAO.getQueuesSeqNextval();
            QueuesDTO queue = new QueuesDTO();
            queue.setQueueManagerId("1");
            queue.setServiceId(1l);
            queue.setTenanIdl("2222");
            queue.setDescription("QUEUE_" + branch.getCode());
            int y = this.branchDAO.createQueues(queue, branchSeqId);
            if (y > 0) {
                //insert queue_parameter khoi tao tham so cau hinh cho queue
                QueueParameterDTO parameter = new QueueParameterDTO();
                parameter.setQueueId(branchSeqId);
                parameter.setParamId(4l);
                parameter.setDescription("init parameter queue " + branch.getName());
//                parameter.setValue(branch.getCode());
                //chuyen sang dung agent id
                parameter.setValue(branchSeqId.toString());
                return this.branchDAO.createQueuesParam(parameter);

            } else {
                //xoa branch vua tao
                this.branchDAO.deleteBranch(branchSeqId);
                return 0;
            }
        } else {
            return 0;
        }
    }

    @Override
    public Integer editBranch(BranchDTO branch) {
        return this.branchDAO.editBranch(branch);
    }

    @Override
    public boolean checkAgentExist(BranchDTO agentInfo) {
        if (this.branchDAO.CheckAgentCodeExists(agentInfo) > 0) {
            return true;
        }
        return false;
    }

    @Override
    public Integer getCountBranchDetail(BranchDetailQueryDTO query) {
        return this.branchDAO.getCountBranchDetail(query);
    }

    @Override
    public List<ContactDTO> getBranchDetail(BranchDetailQueryDTO query) {
        return this.branchDAO.getBranchDetail(query);
    }

    @Override
    public Integer addContact(ContactDTO query) {

        query.setMsisdn(Utilities.preparePhoneNums(query.getMsisdn()));
        //khoi tao user
        Long userId = this.accountDAO.getUserSeqNextval();
        //tao account cho cu dan. tach sang vong lap khac khong de anh huong den toa nha
        //lay thong tin toa nha
        List<BranchDTO> branchInfo = getById(query.getAgentId());
        BaseResult createAccountResult = new BaseResult();
        int addContactResult = 0;
        if (branchInfo != null && branchInfo.size() > 0) {
            //craete account
            //chua tinh toi truong hop mot so dien thoai register o 2 toa nha. confirm lai sau
            AccountDTO cusAccount = new AccountDTO();
            cusAccount.setUsername(branchInfo.get(0).getCode() + "_" + query.getMsisdn());
            //fix mat khau mac dinh la 123
            cusAccount.setPassword("123");
            cusAccount.setRePassword("123");
            cusAccount.setAddress(query.getDescription());
            cusAccount.setAgentId(query.getAgentId());
            cusAccount.setFirstName(query.getContactName());
            cusAccount.setPhone(query.getMsisdn());
            cusAccount.setRole("3");//nguoi dan
            cusAccount.setId(userId);
            createAccountResult = this.accountService.createAccount(cusAccount);
        }
        if (!createAccountResult.isIsFailed()) {
            query.setUserId(userId);
            addContactResult = this.branchDAO.addContact(query);
        }
        return addContactResult;
    }

    @Override
    public Integer changeContactStatus(Long id, String status) {
        return this.branchDAO.changeContactStatus(id, status);
    }

    @Override
    public void saveContactBatch(Long branchId, List<ContactDTO> listContact) {
        this.branchDAO.saveContactBatch(branchId, listContact);
    }

    @Override
    public String makeCall(Long branchId, String notifyContent, AccountDTO userInfo) {
        try {
            //check neu truoc do 2 tieng toa nha da co canh bao chay thi thoi, khong insert nua
            if (this.callOutDAO.checkEventLastTwoHours(branchId) > 0) {
                return "ERR_LAST_2_HOUR";
            }
            //Noi dung canh bao mac dinh
            if (Strings.isNullOrEmpty(notifyContent)) {
                notifyContent = "Toa nha dang co chay. Yeu cau tat ca moi nguoi so tan!";
            }
            Long id = this.callOutDAO.getCallOutEventSeqNextval();
            BranchDetailQueryDTO query = new BranchDetailQueryDTO();
            query.setObjId(branchId);
            query.setIsActive("Y");
            query.setPhoneNumberExclude(userInfo.getPhone());
            List<ContactDTO> listContact = this.branchDAO.getBranchDetail(query);

            //insert ban ghi vao bang call_out_event
            CallOutEventDTO event = new CallOutEventDTO();
            event.setAgentId(branchId);
            event.setCreatedBy(userInfo.getId());
            event.setCreatedDate(new Date());
            event.setUpdateBy(userInfo.getId());
            event.setUpdateDate(new Date());
            event.setTotalRow(Long.valueOf(String.valueOf(listContact.size())));
            event.setTotalCommitRow(0l);
            event.setStatus(Constant.CallOutEventStatus.INIT);
            event.setNotifyContent(notifyContent);
            this.callOutDAO.makeCallOutEvent(event, id);

            //insert batch vao  bang poke call
            List<PlayerDTO> toSave = genPlayDTO(listContact, notifyContent, userInfo);
            insertPlayerBatch(toSave, id);
            return "SUCCESS";
        } catch (Exception ex) {
            return "FALSE";
        }
    }

    public List<PlayerDTO> genPlayDTO(List<ContactDTO> input, String notifyContent, AccountDTO userInfo) {
        List<PlayerDTO> result = new ArrayList();
        Iterator<ContactDTO> iterator = input.iterator();
        Date requestTime = new Date(System.currentTimeMillis());
        int rowNum = 1;
        while (iterator.hasNext()) {
            ContactDTO source = iterator.next();
            if (source.getMsisdn().trim() == null
                    ? userInfo.getPhone().trim() == null
                    : source.getMsisdn().trim().equals(userInfo.getPhone().trim())) {
                //neu la so dt cua user --> khong canh bao
                continue;
            }
            String transId = IUtils.genCallId(source.getMsisdn());
            PlayerDTO playerDTO = new PlayerDTO(transId, "", source.getMsisdn(), requestTime, notifyContent);
            playerDTO.setRowId(rowNum++);
            result.add(playerDTO);
        }
        return result;
    }

    public void insertPlayerBatch(List<PlayerDTO> input, Long insertedId) {
        int count = 0;
        List<PlayerDTO> batchList = new ArrayList<>();
        for (PlayerDTO bo : input) {
            batchList.add(bo);
            if (++count % 1000 == 0) {
                this.lotteryResultDAO.saveBatch(batchList, insertedId);
                batchList.clear();
            }
        }
        if (!batchList.isEmpty()) {
            this.lotteryResultDAO.saveBatch(batchList, insertedId);
        }
    }

    @Override
    public List<CallInfoDTO> getCallInfo(CallInfoQueryDTO query) {
        return this.branchDAO.getCallInfo(query);
    }

    @Override
    public Integer getCountCallInfo(CallInfoQueryDTO query) {
        return this.branchDAO.getCountCallInfo(query);
    }

    @Override
    public Integer deleteContact(Long id) {
        //set user active status
        ContactDTO contactInfo = this.branchDAO.getContactById(id);
        int updateUserResult = 0;
        if (contactInfo != null) {
            List<AccountDTO> accounts = this.accountDAO.getByContactId(id);
            if (accounts.size() > 0) {
                updateUserResult = this.accountService.changeActiveStatus(accounts.get(0), 0);
            }
        } else {
            updateUserResult = 1;
        }
        if (updateUserResult > 0) {
            return this.branchDAO.deleteContact(id);
        }
        return 0;
    }

    @Override
    public Long getUserSeqNextVal() {
        return this.accountDAO.getUserSeqNextval();
    }

    @Override
    public ContactDTO getContactByUserId(Long userId) {
        return this.branchDAO.getContactByUserId(userId);
    }

}
