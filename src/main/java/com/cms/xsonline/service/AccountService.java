/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cms.xsonline.service;

import com.cms.xsonline.base.BaseService;
import com.cms.xsonline.dto.*;
import com.cms.xsonline.obj.BaseResult;
import com.cms.xsonline.obj.exception.FAException;
import com.cms.xsonline.obj.request.FAUpdateRequest;

import java.util.List;

/**
 * @author pxduong
 */
public abstract interface AccountService extends BaseService<AccountDTO, AccountQueryDTO> {
    public BaseResult createAccount(AccountDTO account);

    public BaseResult editAccount(AccountDTO account);

    public List<AccountDTO> getByUserName(String userName);

    public AccountDTO getById(Long id);

    public List<RoleDTO> getRoleByUserId(Long userId);

    public List<BranchDTO> getAllBranch();

    public List<RoleDTO> getAllRole();

    public Integer deleteAccount(Long id);
    
    public Integer changeActiveStatus(AccountDTO id, int status);

    public Integer unlockAccount(Long id);

    public int changePass(AccountDTO accountInfo, ChangePasswordDTO changePass);

    public boolean checkAccountExist(AccountDTO accountInfo);

    public BaseResult updateSuperAgentDiscount(AccountDTO accountId, Float discount);

    //NamNV -- BEGIN 2018.04.24
    int updateProfile(int userId, FAUpdateRequest request) throws FAException;

    int activeNotification(int userId, boolean enableOrNot) throws FAException;
    //NamNV -- END 2018.04.24
    
    public List<AccountDTO> getManagerByAgent(Long agentId);
}
