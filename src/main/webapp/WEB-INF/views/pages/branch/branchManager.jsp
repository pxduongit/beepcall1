<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%--<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%--<%@ taglib uri="http://beepcall.com/jsp/taglibs" prefix="flo"%>--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/xsonline-common.js"/>'></script>
<script type="text/javascript" src='<spring:url value="/resource/js/xsonline/branch.js"/>'></script>

<div>
    <spring:url var="breadcrumbGetHomeUrl" value="/getHome" htmlEscape="true"/>
    <div class="row">
        <div id="breadcrumb" class="col-xs-12">
            <ol class="breadcrumb">
                <li><a href="${breadcrumbGetHomeUrl}">Home</a></li>
                <li>Quản lý tòa nhà</li>
            </ol>
        </div>
    </div>
    <!--<input id="totalPage" type="hidden" value="${totalPage/pageSize}"/>-->
    <div class="form-search-branch well">
        <form:form  id ="searchForm" >
            <table width="100%">
                <tr>
                    <td >
                        <!--<label >Mã đại lý </label>-->
                        <input type="text" id="branchCode" class="form-control searchElement"
                               style="max-width: 95%" placeholder="Nhập mã tòa nhà để tìm kiếm"/>
                    </td>
                    <td  >
                        <!--<label >Mã đại lý </label>-->
                        <input type="text" id="branchName" class="form-control searchElement"
                               style="max-width: 95%" placeholder="Nhập tên tòa nhà để tìm kiếm"/>
                    </td>
                    <!--                    <td>
                                            <div class="control-group" style="max-width: 95%">
                                                <label for="txtFromDate" class="control-label">Từ ngày</label>
                                                <div class="controls">
                                                    <div class="input-group">
                                                        <input id="txtFromDate" type="text" class="date-picker form-control searchElement" placeholder="Created from"/>
                                                        <label  for="txtFromDate" class="input-group-addon btn">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>-->
                    <!--                    <td>
                                            <div class="control-group" style="max-width: 95%">
                                                <label for="txtToDate" class="control-label">Đến ngày</label>
                                                <div class="controls">
                                                    <div class="input-group">
                                                        <input  id="txtToDate" type="text" class="date-picker form-control searchElement" placeholder="To"/>
                                                        <label   for="txtToDate" class="input-group-addon btn">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>-->
                </tr>
                <tr>
                <input type="hidden"
                       name="${_csrf.parameterName}"
                       value="${_csrf.token}"/>
                <td align="left">
                    <button id="btnSearchBranch" class=" btn btn-warning btn-sm" type="button" style="margin-top: 5px">
                        <span><i class="fa fa-clock-o "></i></span>Search<span class="glyphicon glyphicon-search" style="margin-left: 5px"></span>
                    </button>
                </td>
                <td></td>
                <td></td>
                </tr>
            </table>
        </form:form>
    </div>
    <!--form-list-branch    -->
    <div id="form-list-branch">
        <div class="box-content no-padding">
            <div style="float:right; margin: auto">
                <button 
                    data-toggle="modal" 
                   title="add Building"
                   href="#myModalAddBuilding"
                    id="btnAddNewBranch" 
                    class="btn btn-success  btn-sm" type="button" style="margin-bottom: 5px">
                    <span><i class="fa fa-clock-o "></i></span> Thêm mới tòa nhà <span class="glyphicon glyphicon-plus" style="margin-left: 5px"></span>
                </button>
            </div>
            <table id="branchListTable"
                   class="table table-bordered table-striped table-hover table-heading table-datatable dataTable">
                <thead>
                    <tr>
                        <th class="hidden-xs col-sm-1 ">STT</th>
                        <th class="hidden-xs col-sm-1 ">Mã tòa nhà</th>
                        <th class="col-xs-4 col-sm-2 ">Tên tòa nhà</th>
                        <th class="col-xs-4 col-sm-3 ">Địa chỉ</th>						       
                        <th class="col-xs-4 col-sm-2 ">&nbsp;</th>
                    </tr>
                </thead>
                <tbody  id="tableBody" >
                    <c:set var="rowIndex" value="1"/>
                    <c:forEach var="branch" items="${listBranch}">
                        <tr>

                            <td  style="text-align: center"><c:out value="${((curPage-1)*pageSize)  + rowIndex}"/></td>
                            <td  style="text-align: left" onclick="window.location = contextPath + '/agent/getBranchDetail/' +${branch.id}"><a href="#"><c:out value="${branch.code}"/></a></td>
                            <td  style="text-align: left" ><c:out value="${branch.name}"/></td>
                            <td  style="text-align: left"><c:out value="${branch.addr}"/></td>
                            <spring:url value="/agent/getEditBranch/" var="editUrl" htmlEscape="true"/>
                            <spring:url value="/doDeleteBranch/" var="deleteUrl" htmlEscape="true"/>
                            <td class="col-xs-4 col-sm-2" style="text-align: center;">
                                <a style="margin-right: 5px" href="${editUrl}<c:out value="${branch.id}"/>">Edit</a>

                                <a data-toggle="modal" 
                                   data-id="${branch.code.concat(';').concat(branch.id)}"
                                   title="Confirm delete agent" class="open-AddBookDialog " 
                                   href="#myModal"
                                   >Delete</a>
                            </td>
                            <c:set var="rowIndex" value="${rowIndex+1}"/>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>

            <!--paging-->
            <c:set var="pagingPrefix" value="/agent/getBranch"/>
            <%@include file="/WEB-INF/views/pages/layout/paginition.jsp" %>
        </div>
    </div>

</div>

<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Are you sure?</h4>
            </div>
            <div class="modal-body">
                <label>Bạn có chắc chắn muốn xóa tòa nhà  <label class="object-name" style="color: red"></label>?</label>
                <input type="hidden" class="object-id" id="delAgentId"/>
                <button type="button" class="btn btn-default" id="btnConfirmDeleteAgent">Yes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

            </div>
        </div>

    </div>
</div>

<!-- Modal Add Building -->
<div id="myModalAddBuilding" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Thêm mới tòa nhà</h4>
            </div>
            <div class="modal-body">
                <spring:url value="/addAgent" htmlEscape="true"
                            var="formActionUpdate"></spring:url>
                <form:form class="form-horizontal" role="form"
                           action="${formActionUpdate}" id="addBuildingForm" modelAttribute="buildingInfo">
                    <div class="form-group has-feedback control-group">
                        <label class="col-sm-4 control-label">Tên tòa nhà</label>
                        <div class="col-sm-8">
                            <input type="text" id="buildingName" path="buildingName" name="buildingName"
                                   class="form-control" placeholder="Tên tòa nhà"/>
                        </div>
                    </div>
                    <div class="form-group has-feedback control-group">
                        <label class="col-sm-4 control-label">Địa chỉ</label>
                        <div class="col-sm-8">
                            <input type="text" id="buildingAddress" path="buildingAddress" name="buildingAddress"
                                   class="form-control" placeholder="Địa chỉ tòa nhà"/>
                        </div>
                    </div>
                    <div class="form-group control-group" style="margin-bottom: 0px">
                        <button onclick="addBuilding()" style="float: right;margin-left: 5px ;margin-right: 15px" type="button" class="btn btn-default " id="btnAddBuilding">Save</button>
                        <button id="hideModalAddContact" style="float: right" type="button" class="btn btn-default " data-dismiss="modal">Close</button>
                    </div>
                    <!--</div>-->
                </form:form>
            </div>
        </div>

    </div>
</div>