/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    if ($('#pageType').val() !== 'edit') {
        AccountFormAddValidator();
    } else {
        AccountFormEditValidator();
    }

    $("#btnSearchAccount").on('click', function () {
        $("#curPage").val(1);
        $("#pageSize").val(15);
        Search();
    });

    $('#btnAddNewAccount').on('click', function () {
        $(location).attr('href', contextPath + '/account/getAddNewAccount');

    });
    $("#btnConfirmDelete").on("click", function () {
        var id = $("#delAccountId").val().trim();
        if(id==$('#currAccountId').val().trim()){
        //truong hop xoa account hien tai
            alert('Không được xóa account hiện tại');
            return false;
        }
        $(location).attr('href', contextPath + '/account/doDeleteAccount?id=' + id);
    });

    $("#btnConfirmUnlock").on("click", function () {
        $(location).attr('href', contextPath + '/account/doUnlockAccount?id=' + $("#unlockAccountId").val());
    });

    $('#chkChangePass').click(function () {
        $("#changePassOnEdit").toggle(this.checked);
    });
    if ($('#pageType').val() == 'edit') {
        $('#changePassOnEdit').hide();
    }
    //chinh sua phan neu select role != agent thi khong can chon agent
    //$('#agentSelectBlock').hide();
    $('#roleSelect').change(function () {
        var textSelected = $("#roleSelect option:selected").text();
        if (textSelected.toLowerCase() === 'admin') {//if agent
//console.log('1');
            $('#agent_info').hide();
        } else {
//console.log('2');
            $('#agent_info').show();
        }
//        $(this).find(":selected").each(function () {
//            if ($(this).val() != 3) {//if agent
//                $('#agentSelect').val("Select");
//                $('#agentSelectBlock').hide();
//            }
//            else {
//                $('#agentSelect').val("Select");
//                $('#agentSelectBlock').show();
//            }
//        });
    });

    $('.unlock-btn').mouseover(function () {
        $(this).text('Unlock');
    });
    $('.unlock-btn').mouseout(function () {
        $(this).text('Locked');
    });

    $("#discountPercent").attr({
        "max": 100, // substitute your own
        "min": 0          // values (or variables) here
    });
});

$(document).on("click", ".open-AddBookDialog", function () {
    var sourceData = $(this).data('id');
    var id = sourceData.split(';')[1];
    var username = sourceData.split(';')[0];
    $(".modal-body #delAccountId").val(id);
    $(".modal-body #delAccountUsername").html(username);
});

$(document).on("click", ".open-AddBookDialog2", function () {
    var sourceData = $(this).data('id');
    var id = sourceData.split(';')[1];
    var username = sourceData.split(';')[0];
    $(".modal-body #unlockAccountId").val(id);
    $(".modal-body #unlockAccountUsername").html(username);
});

function Search() {
    $('#loading').show();
//    var data = $("#searchForm").serialize();
    var url = contextPath + "/account/getAccountList";
    var username = $("#username").val();
//    var fromDate = $("#txtFromDate").val();
//    var toDate = $("#txtToDate").val();
    var phoneNumber = $("#phoneNumber").val();
    var agentId = $("#agentId").val();
console.log(agentId);
    var agentCode = $("#agentCode").val();
    var agentName = $("#agentName").val();
    var curPage = $("#curPage").val();
    var pageSize = $("#pageSize").val();
    var data = {};
    if (username != undefined && username != "") {
        data.username = username;
    }
    if (phoneNumber != undefined && phoneNumber != "") {
        data.phoneNumber = phoneNumber;
    }
    if (agentId != undefined && agentId != "") {
        data.agentId = agentId;
    }
    if (agentCode != undefined && agentCode != "") {
        data.agentCode = agentCode;
    }
    if (agentName != undefined && agentName != "") {
        data.agentName = agentName;
    }
//    if (fromDate != undefined && fromDate != "") {
//        data.fromDate = fromDate;
//    }
//    if (toDate != undefined && toDate != "") {
//        data.toDate = endDayOfDate(toDate);
//    }
    data.curPage = curPage;
    data.pageSize = pageSize;
    $.ajax({
        url: url,
        data: data,
        dataType: 'json',
        type: 'GET',
        success: function (result) {
//            jQuery.parseJSON(e);
            $('#loading').hide();
            var curPage = result.curPage;
            var pageSize = result.pageSize;
            var totalRecord = result.totalRecord;
            bindingTable(result);
            updatePaginition(curPage, pageSize, totalRecord);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            $('#loading').hide();
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        }
    });
}

function bindingTable(result) {
    var account = result.listData;
    var tr;
    var index = 1;
    $("#tableBody").html("");

    for (var i = 0; i < account.length; i++) {
        tr = $('<tr/>');
        tr.append("<td style=\"text-align: center\">" + (((result.curPage - 1) * result.pageSize) + index) + "</td>");
        tr.append("<td style=\"text-align: left\">" + removeNull(account[i].username) + "</td>");
        tr.append("<td style=\"text-align: left\">" + removeNull(account[i].roleName) + "</td>");
        tr.append("<td style=\"text-align: left\">" + removeNull(account[i].email) + "</td>");
        tr.append("<td style=\"text-align: left\">" + removeNull(account[i].phone) + "</td>");
        tr.append("<td style=\"text-align: left\">" + removeNull(account[i].address) + "</td>");
        tr.append("<td style=\"text-align: left\">" + removeNull(account[i].agentName) + "</td>");
//        var td = $('<td style="text-align: center;"/>');
//        if (account[i].enable) {
//            td.append("<span class=\"label label-success\">OK</span>");
//        } else {
//            td.append("<span class=\"label label-danger\">Lock</span>");
//        }
//        tr.append(td);
        var td = $('<td style="text-align: center;"/>');
        var editUrl = escapeHtml(contextPath + "/account/getEditAccount/" + account[i].id);
        var deleteData = escapeHtml(account[i].username + ";" + account[i].id);
        if (account[i].enable) {
            td.append("<a  style=\"margin-right: 5px\" href=\"" + editUrl + "\">Edit</a>");
            td.append("<a data-toggle=\"modal\" data-id=\"" + deleteData + "\"title=\"Confirm delete account\" class=\"open-AddBookDialog\" href=\"#myModal\">Delete</a>");
        }
        tr.append(td);
        $("#tableBody").append(tr);
        index++;
    }
}
function AccountFormAddValidator() {
    $('#accountInfoForm').bootstrapValidator({
        message: 'Dữ liệu không hợp lệ',
        excluded: [':disabled', ':invisible'],
        fields: {
            username: {
                validators: {
                    notEmpty: {
                        message: 'Không được để trống username'
                    },
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: 'Không được để trống số điện thoại'
                    },
                }
            },
            email: {
                validators: {
                    notEmpty: {
//                        message: 'The email is not correct format'
                        message: 'Không được để trống email'
                    },
                }
            },
            password: {
                validators: {
                    notEmpty: {
                        message: 'Không được để trống mật khẩu'
                    },
//                    regexp: {
//                        regexp: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/,
//                        message: 'Mật khẩu phải có ít nhất 8 ký tự. Ít nhất một ký tự thường, một ký tự viết hoa và một ký tự số.'
//                    },
//                    identical: {
//                        field: 'rePassword',
//                        message: 'Password not match'
//                    }

                }
            },
            rePassword: {
                validators: {
//                    regexp: {
//                        regexp: /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/,
//                        message: ''
//                    },
                    identical: {
                        field: 'password',
                        message: 'Mật khẩu không khớp'
                    },
                }
            },
            agentId: {
                validators: {
                    notEmpty: {
                        message: 'Không được để trống tòa nhà'
                    },
                }
            },
            role: {
                validators: {
                    notEmpty: {
                        message: 'Bạn chưa chọn quyền cho account'
                    },
                }
            },
        }
    });
}
function AccountFormEditValidator() {
    $('#accountInfoForm').bootstrapValidator({
        message: 'Dữ liệu không hợp lệ',
        excluded: [':disabled', ':invisible'],
        fields: {
            username: {
                validators: {
                    notEmpty: {
                        message: 'Không được để trống username'
                    },
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: 'Không được để trống số điện thoại'
                    },
                }
            },
            email: {
                validators: {
                    notEmpty: {
//                        message: 'The email is not correct format'
                        message: 'Không được để trống email'
                    },
                }
            },
        }
    });
}




//    $('<div>asdfadfaf</div>').appendTo('body')
//            .html('<div><h6>Yes or No?</h6></div>')
//            .dialog({
//                modal: true, title: 'message', zIndex: 10000, autoOpen: true,
//                width: 'auto', resizable: false,
//                buttons: {
//                    Yes: function () {
//                        doFunctionForYes();
//                        $(this).dialog("close");
//                    },
//                    No: function () {
//                        doFunctionForNo();
//                        $(this).dialog("close");
//                    }
//                },
//                close: function (event, ui) {
//                    $(this).remove();
//                }
//            });


